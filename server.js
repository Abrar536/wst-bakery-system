var express = require("express");
var session = require("express-session");
var app = express();
var path  = require("path");
var mysql = require('mysql2');
var bodyParser = require('body-parser');
var jsdom = require("jsdom");
var JSDOM = jsdom.JSDOM;
global.document = new JSDOM('cakes.html').window.document;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


var con = mysql.createConnection({
                host: "localhost",
                user: "newuser",
                password: "password",
		database:"bakeryshop"
});
con.connect(function(err) {
                if (err) throw err;
                console.log("Connected!");
});
 
app.use(session({ secret: 'WST-PROJECT'}));
app.use(express.static(__dirname + "/public"));


//HOME PAGE!!!
app.get("/", function (request, response){
     response.sendFile(__dirname+"/views/home.html");
});

app.get("/signup1", function (request, response){
     response.sendFile(__dirname+"/views/sign-up.html");
});

app.get("/login1", function (request, response){
     response.sendFile(__dirname+"/views/login.html");
});

app.get("/cakes1", function (request, response){
     response.sendFile(__dirname+"/views/cakes.html");
});

app.post("/signup", function (request, response){
        var name = request.body.name;
        var password = request.body.password;
	var password2 = request.body.password2;
	var email = request.body.email;
	var contact = request.body.contact;
	var addr = request.body.addr;
        console.log(email);
        console.log(password);
        con.connect(function(err) {
                if (err) throw err;
                console.log("Connected!");
                var sql = "INSERT INTO Users (cname, email, contact, passwd, address) VALUES ('"+name+"','"+email+"','"+contact+"','"+password+"','"+addr+"')";
                con.query(sql, function (err, result) {
                        if (err) throw err;
                        console.log("New record inserted into Users table");
                });
		var log = "INSERT INTO LoginCredentials (username, pass_word) VALUES ('"+email+"','"+password+"')";
                con.query(log, function (err, result) {
                        if (err) throw err;
                        console.log("New record inserted into LoginCredentials table");
                });
        });
	//start session and render the home page 
	response.redirect('/');
	
});

app.post("/login", function (request, response){
	var email = request.body.email;
    	var password = request.body.password;
	console.log(email);
	console.log(password);

	if (email && password) {
		var sql = "SELECT * from LoginCredentials where username='"+email+"'";
		con.query(sql, function(error, results, fields) {
			if (results.length > 0) {
				request.session.loggedin = true;
				request.session.cid = results[0].cid;
				request.session.password = results[0].pass_word;
				request.session.email = results[0].email;
				response.redirect("/cakes1");
			} else {
				response.send('Incorrect Username and/or Password!');
			}
		});
	} else {
		response.send('Please enter Username and Password!');
		response.end();
	}

});

app.get('/logout', function (request, response){
	request.session.destroy(function(err) {
      		response.redirect("/");
	});
});

app.post('/confirm', function (request, response){
	var res = request.app.get('res');
	var cid = request.session.cid;
	var cardnum = request.body.cardnum;
	console.log(cardnum);
	var order_date = new Date().toISOString().slice(0, 19).replace('T', ' ');
	var price = res[0].price;
	var cakeid = res[0].cakeid;
	var order = "INSERT INTO Orders(cid, cardnum, order_date, cakeid, price) VALUES ('"+cid+"','"+cardnum+"','"+order_date+"','"+cakeid+"','"+price+"')";
	con.query(order, function (err, result) {
                if (err) throw err;
		console.log("inserted an order!");
	});
	var elem = "SELECT * from Users where cid="+cid+"";	
	con.query(elem, function (err, result) {
                if (err) throw err;
		response.render(__dirname+"/views/receipt.ejs", {result:res, date:order_date, name:result[0].cname});
	});
	
});

app.get('/receipt', function (request, response){
	response.render(__dirname+"/views/receipt.ejs", {result:res, date:order_date});
});

app.get("/cart-add", function (request, response){
	var id = parseInt(request.query.id);
	var elem = "SELECT * from Cakes where cakeid="+id+"";
	var res;
        con.query(elem, function (err, result) {
                if (err) throw err;
		console.log(result[0].cakeid);
		console.log(result[0].price);
		console.log(result[0].cname);
		request.app.set('res', result)
		response.render(__dirname+"/views/cart.ejs", {result:result});
	});
});


 
//start the server
app.listen(8080);
 
console.log("Running the Bakery shop at http://localhost:8080");
