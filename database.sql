create database bakeryshop;
use bakeryshop;

create table Users
	(cid			INTEGER AUTO_INCREMENT, 
	 cname			VARCHAR(80) not null, 
	 email			VARCHAR(20), 
	 contact		CHAR(10),
	 passwd		VARCHAR(20),
	address		VARCHAR(20),
	PRIMARY KEY 		(cid)
	);


create table LoginCredentials
	(cid			INTEGER AUTO_INCREMENT,
	 username		VARCHAR(30) not null,
	 pass_word 		VARCHAR(20) not null unique,
	 PRIMARY KEY 		(cid)
	);

create table Cakes
	(cakeid		INTEGER,
	 cname			VARCHAR(80),
	 price			REAL,
	 PRIMARY KEY 		(cakeid)
	);

create table Orders
	(ordernum	INTEGER AUTO_INCREMENT,
	 cid		INTEGER,
	 cardnum 	CHAR(16),
	 order_date	DATE,
	cakeid		INTEGER,
	price		REAL,
	 PRIMARY KEY    (ordernum),
	 FOREIGN KEY    (cid) REFERENCES Users(cid),
	FOREIGN KEY    (cakeid) REFERENCES Cakes(cakeid)
	); 



insert into Cakes values(1,"Vegan Black Forest Cake",349);
insert into Cakes values(2,"Chocolate choco-chip Cake", 520);
insert into Cakes values(3,"RoundVegan Coffee Cake",380);
insert into Cakes values(4,"Classic RoundVegan Vanilla Cake",480);
insert into Cakes values(5,"Tiramisu Kitkat Cheese Cake",500);
insert into Cakes values(6,"RoundVanilla N Choco Web Irish Cake",330);
insert into Cakes values(7,"Delicious Moist Chocolate Cake",420);
insert into Cakes values(8,"Floral Chocolate andCaramel Cake",520);
insert into Cakes values(9,"Ferrero Rocher Cake",480);
insert into Cakes values(10,"Chocolate N Nut Cake",390);
insert into Cakes values(11,"Sinful Chocolate Truffle Cake",400);
insert into Cakes values(12,"Cream Cake withCaramel N Pineapple",480);
insert into Cakes values(13,"Mango Cheese Cake",350);
insert into Cakes values(14,"Red Velvet Creame Cake",420);
insert into Cakes values(15,"Classic Oreo Delight",400);
insert into Cakes values(16,"White Bread",40);
insert into Cakes values(17,"Brown Bread",60);
insert into Cakes values(18,"Multigrain Bread",75);
insert into Cakes values(19,"Pita Bread",115);
insert into Cakes values(20,"Banana Bread",140);
insert into Cakes values(21,"Garlic Bread",175);
insert into Cakes values(22,"Choco-chip cookies",220);
insert into Cakes values(23,"Cashewnut Cookies",250);
insert into Cakes values(24,"Oatmeal Cookies",300);
insert into Cakes values(25,"Peanut Cookies",200);
insert into Cakes values(26,"Fortune Cookies",350);
insert into Cakes values(27,"Sugar Cookies",375);

